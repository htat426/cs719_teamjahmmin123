// separate file for database access functions @alan

var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('blog-db.db');

//allows us to create a nice looking date and time
var dateTime = require('node-datetime');

module.exports.getUser = function (username, callback) {
    db.all("select * from Users where username = ?", [username], function (err, rows) {
        if (rows.length > 0) {
            callback(rows[0]);
        } else {
            callback(null);
        }
    });
}

module.exports.createUser = function (u, callback) {
    db.run("insert into Users (username, password, dob, country, avatar, fname, lname, activeFlag) values (?, ?, ?, ?, ?, ?, ?, ?)",[u.username, u.password, u.dob, u.country, u.avatar, u.fname, u.lname, u.activeFlag], function (err) {
        if (err) {
            console.log(err);
        }
    });
    callback();
}

module.exports.getAllArticles = function (callback) {
    db.all("select * from Articles order by timestamp desc", function (err, rows) {
        callback(rows);
    });
}

module.exports.createArticle = function (a, callback) {

    var date = dateTime.create();
    var timestamp = date.format("Y-m-d H:M");

    db.run("insert into Articles (author, timestamp, title, content, image, video, link) values (?, ?, ?, ?, ?, ?, ?)", [a.username, timestamp, a.title, a.content, a.image, a.video, a.link], function (err) {
        if (err) {
            console.log(err);
        }
    });
    callback()
}
